package br.pucpr.ticketsAereos.tests.daoModification.crud;

import org.junit.Assert;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.CiaAereaBC;
import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.tests.bc.crud.TesteCiaAereaBC;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteCiaAereaDAOModification extends TesteCiaAereaBC {
	
	//////////////////////////////////////////
	// CONSTRUTORES
	//////////////////////////////////////////
	
	public TesteCiaAereaDAOModification(MassaTestesCiaAereaEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Metodo responsavel por inserir uma cia aerea na base de dados
	 */
	@Test
	@Override
	public void criar(){
		//Realiza o INSERT na chamada do metodo para o BC
		//DICA: Caso não esteja rodando aqui, veja se voce esta chamando a classe DAO a partir da BC
		super.criar();
		
		//Verifica se o id eh maior que zero
		Assert.assertTrue(object.getId() > 0);

		//Obtem o objeto do BD a partir do ID gerado
		CiaAerea objetoBD = CiaAereaBC.getInstance().findById(object.getId());
		
		//Realiza as verificacoes
		verificator.verify(objetoBD, object);
	}
	
	/**
	 * Metodo responsavel por atualizar uma cia aerea na base de dados
	 */
	@Test
	public void atualizar(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Atualiza Atributos
		creator.update(object, "-U");
		
		//Atualiza o BD
		CiaAereaBC.getInstance().update(object);
		
		//Obtem o objeto do BD a partir do ID gerado
		CiaAerea objetoBD = CiaAereaBC.getInstance().findById(object.getId());
		
		//Realiza as verificacoes
		verificator.verify(objetoBD, object);
		
		//Atualiza o BD
		CiaAereaBC.getInstance().delete(object);
	}
	
	
	/**
	 * Metodo responsavel por deletar uma cia aerea da base de dados
	 */
	@Test
	public void deletar(){
		//Realiza o INSERT na chamada do metodo para o BC
		criar();
		
		//Remove o objeto do BD
		CiaAereaBC.getInstance().delete(object);
		
		//Obtem o objeto do BD a partir do ID gerado
		CiaAerea objetoBD = CiaAereaBC.getInstance().findById(object.getId());
		
		//Verifica se o objeto deixou de existir no BD
		Assert.assertNull(objetoBD);
	}
}
