package br.pucpr.ticketsAereos.tests.bc.crud;

import org.junit.Test;

import br.pucpr.ticketsAereos.bc.CiaAereaBC;
import br.pucpr.ticketsAereos.exception.BSIException;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;
import br.pucpr.ticketsAereos.tests.model.crud.TesteCiaAereaModel;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 *
 */

public class TesteCiaAereaBC extends TesteCiaAereaModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteCiaAereaBC(MassaTestesCiaAereaEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida ciaAerea completa
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		CiaAereaBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida ciaAerea nula
	 */
	@Test(expected = BSIException.class)
	public void validarCiaAereaNula(){
		CiaAereaBC.getInstance().insert(null);
	}
	
	/**
	 * Valida ciaAerea preenchida com espacos em branco os campos String
	 */
	@Test(expected = BSIException.class)
	public void validarCiaAereaEspacosBranco(){
		object.setNome("                         ");

		CiaAereaBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida ciaAerea sem o campo nome
	 */
	@Test(expected = BSIException.class)
	public void validarCiaAereaSemNome(){
		object.setNome(null);
		CiaAereaBC.getInstance().insert(object);
	}
}
