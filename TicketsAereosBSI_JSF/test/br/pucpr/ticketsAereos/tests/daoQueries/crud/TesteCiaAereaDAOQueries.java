package br.pucpr.ticketsAereos.tests.daoQueries.crud;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.pucpr.ticketsAereos.bc.CiaAereaBC;
import br.pucpr.ticketsAereos.dto.CiaAereaDTO;
import br.pucpr.ticketsAereos.model.CiaAerea;
import br.pucpr.ticketsAereos.tests.massaTestes.MassaTestesCiaAereaEnum;
import br.pucpr.ticketsAereos.tests.verificador.CiaAereaVerificator;

/**
 * Essa classe eh um teste JUnit para a disciplina de programacao do curso de
 * BSI da PUCPR
 * 
 * @author Mauda
 * 
 */

public class TesteCiaAereaDAOQueries extends AbstractCrudTestDAOQueries<CiaAerea, CiaAereaDTO, CiaAereaBC, MassaTestesCiaAereaEnum> {
	
	///////////////////////////////////////////////////////////
	// Construtor
	///////////////////////////////////////////////////////////
	public TesteCiaAereaDAOQueries() {
		verificator = CiaAereaVerificator.getInstance();
		bc = CiaAereaBC.getInstance();
	}
	
	///////////////////////////////////////////////////////////
	// Metodos Abstratos - Implementacao
	///////////////////////////////////////////////////////////
	@Override
	protected int getEnumSize() {
		return MassaTestesCiaAereaEnum.values().length;
	}	
	
	// ////////////////////////////////////////
	// METODOS AUXILIARES
	// ////////////////////////////////////////
	@Before
	public void init() {
		this.filter = new CiaAereaDTO();
	}	
	
	///////////////////////////////////////////////////////////
	// DADOS DA CIAAEREA
	///////////////////////////////////////////////////////////
	@Test
	public void testFindByFilterNome(){
		for (MassaTestesCiaAereaEnum ciaAereaEnum : MassaTestesCiaAereaEnum.values()) {
			//Seta a informacao do filtro
			filter.setNome(ciaAereaEnum.getNome());
			
			//Obtem as informacoes do banco de dados
			List<CiaAerea> ciasFilter = (List<CiaAerea>) bc.findByFilter(filter);
			
			Assert.assertEquals(ciasFilter.size(), 1);
			
			//Verifica se os objetos sao iguais
			verificator.verify(ciasFilter.get(0), ciaAereaEnum);
		}
	}
}
